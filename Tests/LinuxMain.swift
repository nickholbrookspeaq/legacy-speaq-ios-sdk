import XCTest

import speaq_ios_sdkTests

var tests = [XCTestCaseEntry]()
tests += speaq_ios_sdkTests.allTests()
XCTMain(tests)
