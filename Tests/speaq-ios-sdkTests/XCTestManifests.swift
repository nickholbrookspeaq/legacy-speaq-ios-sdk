import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(speaq_ios_sdkTests.allTests),
    ]
}
#endif
